<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Routing\Controller as BaseController;
use App\TAD\TADFactory;

class WhoIsHereController extends BaseController {

    private $START_WORK;
    private $END_WORK;
    private $BREAK_START;
    private $BREAK_END;
    private $UNKNOWN;

    public function __construct()
    {
        $this->START_WORK = env('START_WORK');
        $this->END_WORK = env('END_WORK');
        $this->BREAK_START = env('START_BREAK');
        $this->BREAK_END = env('END_BREAK');
        $this->UNKNOWN = env('UNKNOWN');
    }

    public function here()
    {
        $tad = (new TADFactory(['ip' => env('TAD_PHP_IP')]))->get_instance();

        $today = Carbon::today()->toDateString();

        $logs = $tad->get_att_log();
        $logs = $logs->filter_by_date([
            'start' => $today,
            'end' => $today
        ]);
        $logs = json_decode($logs->to_json());
        $logs = $logs->Row;

        usort($logs, function($a , $b) {
           return $a > $b;
        });

        $people = json_decode($tad->get_all_user_info()->to_json());

        $people = $people->Row;

        $names = [];
        foreach($people as $person) {
            $names[$person->PIN] = $person->Name;
        }

        $notCheckedIn = $names;

        $here = [];

        foreach($logs as $log) {
            if(isset($names[$log->PIN])) {
                $here[$names[$log->PIN]] = $log->WorkCode;
                unset($notCheckedIn[$log->PIN]);
            }
        }

        $break = [];
        $left = [];
        $unknown = [];

        foreach($here as $person => $status) {
            switch ($status) {

                CASE $this->END_WORK:
                    $left[] = $person;
                    unset($here[$person]);
                break;

                case $this->BREAK_START:
                    $break[] = $person;
                    unset($here[$person]);
                break;


                case $this->UNKNOWN:
                    $unknown[] = $person;
                    unset($here[$person]);
                break;

            }
        }


        $haveLeftMessage = "";
        if (count($left)) {
            $hashave = count($left) && count($left) > 1 ? " have " : " has ";
            $haveLeftMessage = join(", ", $left) . $hashave .  "probably left. ";
        }

        $onBreakMessage = "";
        if (count($break)) {
            $isare = count($left) && count($left) > 1 ? " are " : " is ";
            $onBreakMessage = join(", ", $break) . $isare . "probably on a break. ";
        }

        $unknownMessage = "";
        if(count($unknown)) {
            $unknownMessage = "Not sure about " . join(", " , $unknown) . ". ";
        }

        $notCheckedInMessage = "";
        if (count($here)) {

            $notCheckedInMessage = join(", ", $notCheckedIn) . " haven't checked in yet. ";
        }
        $message = $haveLeftMessage . $onBreakMessage . $unknownMessage . $notCheckedInMessage . "Everyone else should be here.";

        if (!strlen($haveLeftMessage) && !strlen($onBreakMessage) && !strlen($unknownMessage) && !strlen($notCheckedInMessage)) {
            $message = "Everyone should be here.";
        }
        return response($message);

    }

}